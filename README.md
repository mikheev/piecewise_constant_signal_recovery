# Piecewise constant signal recovery

Алгоритм восстановление сигналов зарегистрированных неидеальной измерительной системой, влияние которой можно описать свёрткой с аппаратной функцией и добавлением шума.

### Текущие реализации

* GCC - `impls/gcc`. Запуск: `make run_main`
* Parallella Epiphany - `impls/epiphany`. Запуск: `make run`
