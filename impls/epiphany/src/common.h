#ifndef COMMON_H__
#define COMMON_H__

#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "./DspLib/MatrixFunctions/arm_math.h"
#include "signal_consts.h"


#define EXT_MEM_BASE (0x8e000000)
#define EXT_MEM_INPUT_DATA_OFFSET  (0x00100000)
#define EXT_MEM_OUTPUT_DATA_OFFSET (0x01000000)
#define INPUT_DATA_CHUNK (0x100)
#define OUTPUT_DATA_CHUNK (0x100)


typedef struct signal_fixed_t {
    uint32_t n_levels;
    uint32_t kernel_size;
    uint32_t n_points;
    float a_kernel[SIGNAL_KERNEL_SIZE];
    float a_matrix[SIGNAL_N_POINTS * SIGNAL_N_POINTS];
    float xi_signal[SIGNAL_N_POINTS];
    float c_levels[SIGNAL_N_LEVELS];
    float x_matrix[SIGNAL_N_POINTS * SIGNAL_N_LEVELS];
    float f_signal[SIGNAL_N_POINTS];
} signal_fixed_t;


#endif // COMMON_H__