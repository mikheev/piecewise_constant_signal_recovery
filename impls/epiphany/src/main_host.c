#define _POSIX_C_SOURCE 199309L

#include <host_bsp.h>
#include <stdio.h>
#include "common.h"
#include "stdlib.h"
#include "e-hal.h"
#include "time.h"
#include "lwlog.h"
#include "piecewise_constant_signal.h"
#include "piecewise_constant_core_functions.h"

#define MAX_JSON_STRING_LEN 2048

static inline double get_time_ms(void) {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t);
    return (double) t.tv_sec * 1000. + (double) t.tv_nsec / 1000000.;
}


static inline void print_arm_matrix(arm_matrix_instance_f32 *matrix, const char * header_str) {
    printf("%s\n", header_str);
    for (int i = 0; i < matrix->numRows; i++) {
        for (int j = 0; j < matrix->numCols; j++) {
            printf("%.2f ", matrix->pData[i * matrix->numCols + j]);
        }
        printf("\n");
    }
}


void put_signal_for_slave(e_mem_t *ext_mem_input_tasks, signal_t *signal, int task_number)
{
    e_write(
        ext_mem_input_tasks,
        0, 0, task_number * INPUT_DATA_CHUNK,
        (void *)signal->x_matrix,
        signal->n_points * signal->n_levels * sizeof(float)
    );
}

void assign_tasks_to_find_optimal_x(signal_t *signal, int row, e_mem_t *ext_mem_input_tasks, int *task_num) {
    for (int i = 0; i < signal->n_levels; i++) {
        for (int j = 0; j < signal->n_levels; j++) {
            if (i == j) {
                signal->x_matrix[row * signal->n_levels + j] = 1; 
            } else {
                signal->x_matrix[row * signal->n_levels + j] = 0;
            }
        }

        if (row > signal->n_points - 4) {
            assign_tasks_to_find_optimal_x(signal, row - 1, ext_mem_input_tasks, task_num);
        } else {
            put_signal_for_slave(ext_mem_input_tasks, signal, *task_num);
            (*task_num)++;
        }
    }
}


int proces_on_slaves(signal_t *signal, int nprocs)
{
    lwlog_info("Process image on %d cores", nprocs);

    int task_num = 0;
    e_mem_t ext_mem_input_tasks, ext_mem_results;

    bsp_init("main_ecore.elf", 0, NULL);
    bsp_begin(nprocs);

    e_alloc(&ext_mem_input_tasks, EXT_MEM_INPUT_DATA_OFFSET, N_TASKS * INPUT_DATA_CHUNK);
    e_alloc(&ext_mem_results, EXT_MEM_OUTPUT_DATA_OFFSET, nprocs * OUTPUT_DATA_CHUNK);

    signal_t output_signal = copy_signal(signal);


    // --- Main part start ---
    double t_start_ms = get_time_ms();
    lwlog_info("0.000 s. Move data to shared memory");
    assign_tasks_to_find_optimal_x(signal, signal->n_points - 1, &ext_mem_input_tasks, &task_num);

    double t_launch_x_calc_ms = get_time_ms();
    lwlog_info("%.3f s. Launch X calculation", (t_launch_x_calc_ms - t_start_ms) / 1000);
    ebsp_spmd();

    double t_get_results_back_ms = get_time_ms();
    lwlog_info("%.3f s. Get results back", (t_get_results_back_ms - t_start_ms) / 1000);
    
    float min_error = 1.e10;
    int min_error_core_id = 0;
    float error = 0;
    for (int i = 0; i < 16; i++)
    {
        e_read(&ext_mem_results, 0, 0, i * OUTPUT_DATA_CHUNK + signal->n_points * signal->n_levels * sizeof(float), (void *)&error, sizeof(float));
        //printf("Core %d: min error = %lf\n", i, error);

        if (error < min_error) {
            min_error = error;
            min_error_core_id = i;
        }
    }
    e_read(&ext_mem_results, 0, 0, min_error_core_id * OUTPUT_DATA_CHUNK, (void *)(output_signal.x_matrix), signal->n_points * signal->n_levels * sizeof(float));

    double t_launch_c_calc_ms = get_time_ms();
    lwlog_info("%.3f s. Calculate c", (t_launch_c_calc_ms - t_start_ms) / 1000);
    evaluate_optimal_c_by_pseudoinversion(&output_signal);

    double t_end_ms = get_time_ms();
    lwlog_info("%.3f s. Calculation done", (t_end_ms - t_start_ms) / 1000);
    // --- Main part end ---
    
    lwlog_info("Total time spent: %.2f ms", t_end_ms - t_start_ms);

    bsp_end(nprocs);

    printf("------- Best result ---------\n");
    // Calculate f
    for (int i = 0; i < output_signal.n_points; i++) {
        output_signal.f_signal[i] = 0;
                     
        for (int j = 0; j < output_signal.n_levels; j++) {
            output_signal.f_signal[i] += output_signal.x_matrix[i * output_signal.n_levels + j] * output_signal.c_levels[j];
        }
    }
    
    
    printf("\n");
    printf("Min error = %lf\n", min_error);
    print_signal(&output_signal);

    free_signal(&output_signal);

    return 0;
}

int main(int argc, char** argv)
{
    int num_slaves = 16;
    lwlog_info("%d cores available", num_slaves);

    lwlog_info("Load input data");
    //signal_t src_signal = load_input_signal_from_file("../../signals/test_signal_no_noise.json");
    signal_t src_signal = load_input_signal_from_file("../../signals/test_signal_noise_std_05.json"); 
    //signal_t src_signal = load_input_signal_from_file("../../signals/test_signal_noise_std_1.json");

    for (int i = 0; i < src_signal.n_points; i++) {
        src_signal.x_matrix[i * src_signal.n_levels + 0] = 1;
    }
    // print_signal(&src_signal);

    proces_on_slaves(&src_signal, num_slaves);
   

    // for (int i = 1; i <= num_slaves; i++)
    // {
        // proces_image_on_slaves(i);
    // }

    // for (int i = 1; i <= num_slaves; i++)
    // {
        // printf("%.2f\n", times_ms[i]);
    // }

    free_signal(&src_signal);

    return 0;
}
