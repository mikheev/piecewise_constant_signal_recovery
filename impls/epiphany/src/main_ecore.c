#include <e_bsp.h>
#include "stdint.h"

//#include "../../../common/sobel_core.h"
#include "common.h"
#include "string.h"
#include "stdbool.h"
#include "signal_consts.h"


float kernel_data_init[] = KERNEL_DATA;
float xi_signal_data_init[] = XI_SIGNAL_DATA;

void print_signal(signal_fixed_t *signal, float error) {
    int sgnl[12];
    for (int i = 0; i < 12; i++)
    {
        sgnl[i] = signal->x_matrix[i * 3] + 2 * signal->x_matrix[i * 3 + 1] + 3 * signal->x_matrix[i * 3 + 2];
    }
    ebsp_message("%d%d%d%d%d%d%d%d%d%d%d%d %lf", sgnl[0], sgnl[1], sgnl[2], sgnl[3], sgnl[4], sgnl[5], sgnl[6], sgnl[7], sgnl[8], sgnl[9], sgnl[10], sgnl[11], error);
}


float error_func(signal_fixed_t *signal) {
    arm_matrix_instance_f32 matrix_a;
    arm_matrix_instance_f32 matrix_x;
    arm_matrix_instance_f32 matrix_ax;
    arm_matrix_instance_f32 matrix_ax_t;
    arm_matrix_instance_f32 matrix_ax_t_ax;
    arm_matrix_instance_f32 matrix_ax_t_ax_inv;
    arm_matrix_instance_f32 matrix_ax_plus;
    arm_matrix_instance_f32 matrix_pi;
    arm_matrix_instance_f32 matrix_i_m_pi;
    arm_matrix_instance_f32 matrix_error_vect;
    arm_matrix_instance_f32 matrix_xi;

    // // #TODO Move this out 
    float32_t matrix_ax_p[SIGNAL_N_POINTS * SIGNAL_N_LEVELS];
    float32_t matrix_ax_t_p[SIGNAL_N_POINTS * SIGNAL_N_LEVELS];
    float32_t matrix_ax_t_ax_p[SIGNAL_N_LEVELS * SIGNAL_N_LEVELS];
    float32_t matrix_ax_t_ax_inv_p[SIGNAL_N_LEVELS * SIGNAL_N_LEVELS];
    float32_t matrix_ax_plus_p[SIGNAL_N_POINTS * SIGNAL_N_POINTS];
    float32_t matrix_pi_p[SIGNAL_N_POINTS * SIGNAL_N_POINTS];
    float32_t matrix_i_m_pi_p[SIGNAL_N_POINTS * SIGNAL_N_POINTS];
    float32_t matrix_error_vect_p[SIGNAL_N_POINTS];

    arm_mat_init_f32(&matrix_a, signal->n_points, signal->n_points, (float32_t *)(signal->a_matrix));
    arm_mat_init_f32(&matrix_x, signal->n_points, signal->n_levels, (float32_t *)(signal->x_matrix));
    arm_mat_init_f32(&matrix_ax, signal->n_points, signal->n_levels, (float32_t *)matrix_ax_p);
    arm_mat_init_f32(&matrix_ax_t, signal->n_levels, signal->n_points, (float32_t *)matrix_ax_t_p);
    arm_mat_init_f32(&matrix_ax_t_ax, signal->n_levels, signal->n_levels, (float32_t *)matrix_ax_t_ax_p);
    arm_mat_init_f32(&matrix_ax_t_ax_inv, signal->n_levels, signal->n_levels, (float32_t *)matrix_ax_t_ax_inv_p);
    arm_mat_init_f32(&matrix_ax_plus, signal->n_levels, signal->n_points, (float32_t *)matrix_ax_plus_p);
    arm_mat_init_f32(&matrix_pi, signal->n_points, signal->n_points, (float32_t *)matrix_pi_p);
    arm_mat_init_f32(&matrix_i_m_pi, signal->n_points, signal->n_points, (float32_t *)matrix_i_m_pi_p);
    arm_mat_init_f32(&matrix_error_vect, signal->n_points, signal->n_points, (float32_t *)matrix_error_vect_p);
    arm_mat_init_f32(&matrix_xi, signal->n_points, 1, (float32_t *)signal->xi_signal);


    // AX = A X
    arm_mat_mult_f32(&matrix_a, &matrix_x, &matrix_ax);
    arm_mat_trans_f32(&matrix_ax, &matrix_ax_t);
    arm_mat_mult_f32(&matrix_ax_t, &matrix_ax, &matrix_ax_t_ax);
    arm_mat_inverse_f32(&matrix_ax_t_ax, &matrix_ax_t_ax_inv);
    arm_mat_mult_f32(&matrix_ax_t_ax_inv, &matrix_ax_t, &matrix_ax_plus);
    arm_mat_mult_f32(&matrix_ax, &matrix_ax_plus, &matrix_pi);

    for (int i = 0; i < signal->n_points; i++) {
        for (int j = 0; j < signal->n_points; j++) {
            matrix_i_m_pi_p[i * signal->n_points + j] = -matrix_pi_p[i * signal->n_points + j];
            if (i == j) {
                matrix_i_m_pi_p[i * signal->n_points + j] += 1.f;
            }
        }
    }

    arm_mat_mult_f32(&matrix_i_m_pi, &matrix_xi, &matrix_error_vect);

    float error = 0;
    for (int i = 0; i < signal->n_points; i++) {
        error += matrix_error_vect_p[i] * matrix_error_vect_p[i];
    }

    return error;
}


bool check_x_is_singular(signal_fixed_t *signal) {
    // return true if x is a singular matrix
    // retuenfalse if x is not singular
    bool no_ones;
    for (int j = 0; j < signal->n_levels; j++) {
        no_ones = true;
        for (int i = 0; i < signal->n_points; i++) {
            if ((int)(signal->x_matrix[i * signal->n_levels + j]) == 1) {
                no_ones = false;
                break;
            }
        }
        if (no_ones) {
            return no_ones;
        }
    }
    return no_ones;
}

// This stupid function is added to avoid stupid and at the same tipe strange optimization issues
#pragma GCC push_options
#pragma GCC optimize ("O0")
int multiply_safe(int a, int b) {
    return a * b;
}
#pragma GCC pop_options


void find_optimal_x(signal_fixed_t *signal, int row, float *min_error, float *x_matrix_min) {
    const int n_levels = signal->n_levels;
    const int level_base = multiply_safe(row, n_levels);
    const int x_n_points = multiply_safe(n_levels, signal->n_points);

    for (int i = 0; i < n_levels; i++) {
        for (int j = 0; j < n_levels; j++) {
            if (i == j) {
                signal->x_matrix[level_base + j] = 1;
            } else {
                signal->x_matrix[level_base + j] = 0;
            }
        }

        if (row > 0) {
            find_optimal_x(signal, row - 1, min_error, x_matrix_min);
        } else {
            if (check_x_is_singular(signal)) {
                continue;
            }

            // X is not singular
            float error = error_func(signal);

            if (error < *min_error) {
                // memcpy does not work under GCC optimization
                *min_error = error;
                for (int k = 0; k < x_n_points; k++) {
                    x_matrix_min[k] = signal->x_matrix[k];
                }
                //memcpy((void *)x_matrix_min, (void *)(signal->x_matrix), signal->n_levels * signal->n_points * sizeof(float));
            }
        }
    }
}

void initialize_global_buffers(signal_fixed_t *signal_global) {
    signal_global->n_levels = SIGNAL_N_LEVELS;
    signal_global->kernel_size = SIGNAL_KERNEL_SIZE;
    signal_global->n_points = SIGNAL_N_POINTS;

    memcpy(signal_global->a_kernel, kernel_data_init, signal_global->kernel_size * sizeof(float));
    
    int half_kernel_size = signal_global->kernel_size / 2;
    for (int i = 0; i < signal_global->n_points; i++) {
        for (int j = 0; j < signal_global->n_points; j++) {
            if (((i - j) >= -half_kernel_size) && ((i - j) <= half_kernel_size)) {
                signal_global->a_matrix[i * signal_global->n_points + j] = signal_global->a_kernel[i - j + half_kernel_size];
            } else {
                signal_global->a_matrix[i * signal_global->n_points + j] = 0;
            } 
        }
    }

    memcpy(signal_global->xi_signal, xi_signal_data_init, signal_global->n_points * sizeof(float));
    memset(signal_global->c_levels, 0, signal_global->n_levels * sizeof(float));
    memset(signal_global->x_matrix, 0, signal_global->n_levels * signal_global->n_points * sizeof(float));
    memset(signal_global->f_signal, 0, signal_global->n_points * sizeof(float));
}


void process_task(int task_number, signal_fixed_t *signal_global, float * x_matrix_min, float *min_error_ptr) {
    // 1. Load base signal to process.
    
    // TODO: COPY also xi
    memcpy(
        (void *)signal_global->x_matrix,
        (void *)(EXT_MEM_BASE + EXT_MEM_INPUT_DATA_OFFSET + task_number * INPUT_DATA_CHUNK),
        signal_global->n_points * signal_global->n_levels * sizeof(float)
    );

    find_optimal_x(signal_global, signal_global->n_points - 1 - 4, min_error_ptr, x_matrix_min);
}


int main() {
    signal_fixed_t signal_global;
    float x_matrix_min[SIGNAL_N_POINTS * SIGNAL_N_LEVELS];
    float min_error = 1.e10;

    bsp_begin();
    initialize_global_buffers(&signal_global);
    int n_procs = bsp_nprocs();
    int my_pid = bsp_pid();
    
    
    //process_task(my_pid, x_matrix_min, &min_error);
    for (int i = my_pid; i < N_TASKS; i+= n_procs)
    {
        //ebsp_message("Process task %d out of %d", i, N_TASKS);
        process_task(i, &signal_global, x_matrix_min, &min_error);
    }
    //ebsp_message("P %d finished", my_pid);


    memcpy(
        (void *)(EXT_MEM_BASE + EXT_MEM_OUTPUT_DATA_OFFSET + bsp_pid() * OUTPUT_DATA_CHUNK),
        (void *)x_matrix_min,
        signal_global.n_levels * signal_global.n_points * sizeof(float)
    );

    memcpy(
        (void *)(EXT_MEM_BASE + EXT_MEM_OUTPUT_DATA_OFFSET + bsp_pid() * OUTPUT_DATA_CHUNK) + signal_global.n_levels * signal_global.n_points * sizeof(float),
        (void *)&min_error,
        sizeof(float)
    );

    //deinitialize_global_buffers();
    bsp_end();

    return 0;
}
