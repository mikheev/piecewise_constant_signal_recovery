#define ARM_MATH_MATRIX_CHECK
#define _POSIX_C_SOURCE 199309L

#include "stdio.h"
#include "stdint.h"
#include "DspLib/MatrixFunctions/arm_math.h"
#include "time.h"
#include "piecewise_constant_signal.h"
#include "piecewise_constant_core_functions.h"

static inline float get_time_ms(void) {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t);
    return (float) t.tv_sec * 1000. + (float) t.tv_nsec / 1000000.;
}


void find_optimal_x(signal_t *signal, int row, double *min_error, signal_t *signal_min) {
    for (int i = 0; i < signal->n_levels; i++) {
        for (int j = 0; j < signal->n_levels; j++) {
            if (i == j) {
                signal->x_matrix[row * signal->n_levels + j] = 1; 
            } else {
                signal->x_matrix[row * signal->n_levels + j] = 0;
            }
        }

        if (row > 0) {
            find_optimal_x(signal, row - 1, min_error, signal_min);
        } else {
            // arm_matrix_instance_f32 matrix_x;
            // arm_mat_init_f32(&matrix_x, signal->n_points, signal->n_levels, (float32_t *)(signal->x_matrix));
            // print_arm_matrix(&matrix_x, "X");
            
            if (check_x_is_singular(signal)) {
                //printf("S");
                continue;
            }

            // X is not singular
            double error = calculate_error_func(signal);

            if (error < *min_error) {
                *min_error = error;
                //printf("QQQ %lf\n", error);
                // TODO memcpy
                for (int j = 0; j < signal->n_levels * signal->n_points; j++) {
                    signal_min->x_matrix[j] = signal->x_matrix[j];
                }
            }   
        }
    }
}


int main(void) {
    //signal_t src_signal = load_input_signal_from_file("../../signals/test_signal_no_noise.json");
    signal_t src_signal = load_input_signal_from_file("../../signals/test_signal_noise_std_05.json"); 
    //signal_t src_signal = load_input_signal_from_file("../../signals/test_signal_noise_std_1.json");
    float t_start_ms, t_end_ms;
    printf("Hello\n");

    // // Verified result. Should be 309.531
    // signal.x_matrix[0 * signal.n_levels + 0] = 1;
    // signal.x_matrix[1 * signal.n_levels + 1] = 1;
    // signal.x_matrix[2 * signal.n_levels + 2] = 1;
    // for (int i = 3; i < signal.n_points; i++) {
    //     signal.x_matrix[i * signal.n_levels + 0] = 1;
    // }


    // Verified result. Should be 9.3367
    src_signal.x_matrix[0 * src_signal.n_levels + 0] = 1;
    src_signal.x_matrix[1 * src_signal.n_levels + 0] = 1;
    src_signal.x_matrix[2 * src_signal.n_levels + 1] = 1;
    src_signal.x_matrix[3 * src_signal.n_levels + 1] = 1;
    src_signal.x_matrix[4 * src_signal.n_levels + 1] = 1;
    src_signal.x_matrix[5 * src_signal.n_levels + 0] = 1;
    src_signal.x_matrix[6 * src_signal.n_levels + 2] = 1;
    src_signal.x_matrix[7 * src_signal.n_levels + 2] = 1;
    src_signal.x_matrix[8 * src_signal.n_levels + 1] = 1;
    src_signal.x_matrix[9 * src_signal.n_levels + 1] = 1;
    src_signal.x_matrix[10 * src_signal.n_levels + 0] = 1;
    src_signal.x_matrix[11 * src_signal.n_levels + 0] = 1;

    //print_signal(&signal);

    //bool x_is_singular = check_x_is_singular(&signal);
    //printf("x is singular: %d\n", x_is_singular);
    double error = calculate_error_func(&src_signal);
    printf("Error: %lf\n", error);


    double min_error = 100000.;
    //find_optimal_x(&signal, src_signal.n_points, &min_error, x_matrix_min);
    
    // Clean X
    for (int i = 0; i < src_signal.n_points; i++) {
        src_signal.x_matrix[i * src_signal.n_levels] = 1;
        for (int j = 1; j < src_signal.n_levels; j++) {
            src_signal.x_matrix[i * src_signal.n_levels + j] = 0;
        }
    }

    signal_t output_signal = copy_signal(&src_signal);
    
    t_start_ms = get_time_ms();
    find_optimal_x(&src_signal, src_signal.n_points - 1, &min_error, &output_signal);
    evaluate_optimal_c_by_pseudoinversion(&output_signal);
    t_end_ms = get_time_ms();
    
    
    // Calculate f
    for (int i = 0; i < output_signal.n_points; i++) {
        output_signal.f_signal[i] = 0;
        for (int j = 0; j < output_signal.n_levels; j++) {
            output_signal.f_signal[i] += output_signal.x_matrix[i * output_signal.n_levels + j] * output_signal.c_levels[j];
        }
    }
    
    printf("\n");
    printf("Min error = %lf\n", min_error);

    print_signal(&output_signal);
    save_signal_to_file(&output_signal, "output_signal.json");
    
    printf("Time_ms: %.2f\n", t_end_ms - t_start_ms);

    free_signal(&src_signal);
    free_signal(&output_signal);
    return 0;
}