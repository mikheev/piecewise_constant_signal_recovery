#include "stdio.h"
#include "stdlib.h"
#include "DspLib/MatrixFunctions/arm_math.h"
#include "conio.h"
#include "time.h"


void test_matrix_inversion_large(void) {
    const int matrix_dim = 1000;
    printf("\n");

    printf("Dim = %d\n", matrix_dim);

    printf("Malloc\n");

    float32_t *matrix_a_arr = (float32_t *)malloc(matrix_dim * matrix_dim * sizeof(float32_t));
    float32_t *matrix_a_copy_arr = (float32_t *)malloc(matrix_dim * matrix_dim * sizeof(float32_t));
    float32_t *matrix_a_inv_arr = (float32_t *)malloc(matrix_dim * matrix_dim * sizeof(float32_t));
    float32_t *matrix_a_a_inv_arr = (float32_t *)malloc(matrix_dim * matrix_dim * sizeof(float32_t));
    float32_t *matrix_a_a_inv_expected_arr = (float32_t *)malloc(matrix_dim * matrix_dim * sizeof(float32_t));

    printf("Initialize matrix\n");
    srand(0);  // Fixed seed
    for (int i = 0; i < matrix_dim; i++) {
        for (int j = 0; j < matrix_dim; j++) {
            matrix_a_arr[i * matrix_dim + j] = rand();
            matrix_a_copy_arr[i * matrix_dim + j] = matrix_a_arr[i * matrix_dim + j];
            if (i == j) {
                matrix_a_a_inv_expected_arr[i * matrix_dim + j] = 1.f;
            } else {
                matrix_a_a_inv_expected_arr[i * matrix_dim + j] = 0.f;
            }
        }
    }

    arm_matrix_instance_f32 matrix_a;
    arm_matrix_instance_f32 matrix_a_copy;
    arm_matrix_instance_f32 matrix_a_inv;
    arm_matrix_instance_f32 matrix_a_a_inv;
    arm_matrix_instance_f32 matrix_a_a_inv_expected;

    arm_mat_init_f32(&matrix_a, matrix_dim, matrix_dim, (float32_t *)matrix_a_arr);
    arm_mat_init_f32(&matrix_a_copy, matrix_dim, matrix_dim, (float32_t *)matrix_a_copy_arr);
    arm_mat_init_f32(&matrix_a_inv, matrix_dim, matrix_dim, (float32_t *)matrix_a_inv_arr);
    arm_mat_init_f32(&matrix_a_a_inv, matrix_dim, matrix_dim, (float32_t *)matrix_a_a_inv_arr);
    arm_mat_init_f32(&matrix_a_a_inv_expected, matrix_dim, matrix_dim, (float32_t *)matrix_a_a_inv_expected_arr);
    
    printf("Inverse\n");
    const int num_iterations = 10;
    clock_t begin = clock();
    for (int i = 0; i < num_iterations; i++) {
        arm_mat_inverse_f32(&matrix_a_copy, &matrix_a_inv);
    }
    clock_t end = clock();
    double elapsed_secs = (float)(end - begin) / (CLOCKS_PER_SEC * num_iterations);
    printf("Inversion took %f s, (%d, %d)\n", (float)elapsed_secs, (int)begin, (int)end);

    printf("Multiply 1\n");
    arm_mat_mult_f32(&matrix_a, &matrix_a_inv, &matrix_a_a_inv);


    printf("Multiply 2\n"); 
    arm_mat_mult_f32(&matrix_a_inv, &matrix_a, &matrix_a_a_inv);

    free(matrix_a_arr);
    free(matrix_a_copy_arr);
    free(matrix_a_inv_arr);
    free(matrix_a_a_inv_arr);
    free(matrix_a_a_inv_expected_arr);
}


int main(void) {
    test_matrix_inversion_large();
    return 0;
}