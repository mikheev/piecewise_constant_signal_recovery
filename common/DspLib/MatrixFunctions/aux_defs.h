#ifndef AUX_DEFS_H__
#define AUX_DEFS_H__

#include "stdint.h"
//#include "cmsis_gcc.h"

#define CUSTOM_GCC 1

#define __INLINE inline


/**
  \brief   Count leading zeros
  \details Counts the number of leading zeros of a data value.
  \param [in]  value  Value to count the leading zeros
  \return             number of leading zeros in value
 */
#define __CLZ             __builtin_clz

#define MAX_SIGNED_NUM(bits) ((1 << (bits -1)) -1)

#define __SSAT(src, bits)   ((src > MAX_SIGNED_NUM(bits)) ? MAX_SIGNED_NUM(bits) : src)

static inline int32_t __QADD(int32_t a, int32_t b)
{
    if (a > 0) {
        if (b > INT32_MAX - a) {
            return INT32_MAX;
        }
    } else if (b < INT32_MIN - a) {
            return INT32_MIN;
    }

    return a + b;
}

static inline int32_t __QSUB(int32_t a, int32_t b)
{
    return __QADD(a, -b);
}

#endif  // AUX_DEFS_H__