#ifndef PIECEWISE_CONSTNT_SIGNAL_H__
#define PIECEWISE_CONSTNT_SIGNAL_H__

#define XJSON_H_IMPLEMENTATION

#include "stdint.h"
#include "stdio.h"
#include "xjson.h"

#define MAX_JSON_STRING_LEN 2048

typedef struct signal_t {
    uint32_t n_levels;
    uint32_t kernel_size;
    uint32_t n_points;
    float *a_kernel;
    float *a_matrix;
    float *xi_signal;
    float *c_levels;
    float *x_matrix;
    float *f_signal;
} signal_t;


static inline void free_signal(signal_t *signal) {
    free(signal->a_kernel);
    free(signal->a_matrix);
    free(signal->xi_signal);
    free(signal->c_levels);
    free(signal->x_matrix);
    free(signal->f_signal);
}


static inline char * read_file_content_to_string(const char* filename) {
    char *buffer = 0;
    size_t length;
    size_t read_length;
    FILE *f = fopen(filename, "rb");

    if (f) {
        fseek(f, 0, SEEK_END);
        length = ftell(f) + 1;  // +1 for '\0'
        fseek(f, 0, SEEK_SET);
        buffer = malloc(length);
        if (buffer) {
            read_length = fread(buffer, 1, length - 1, f);
            if (read_length != length - 1) {
                printf("Error in reading signal from file");
            }
        }
        buffer[length - 1] = '\0';
    }
    fclose(f);
    return buffer;
}


static inline void save_string_to_file(const char * filename, const char *data) {
    FILE *f = fopen(filename, "w");
    
    if (f) {
        fprintf (f, "%s", data);
    }
    fclose(f);
}


static inline signal_t load_input_signal_from_file(const char *filename) {
    char *json_input_str = read_file_content_to_string(filename);
    xjson *json_input = malloc(sizeof(xjson));
    memset(json_input, 0, sizeof(xjson));
    signal_t signal;

    xjson_setup_read(json_input, json_input_str, strlen(json_input_str));


    xjson_object_begin(json_input, NULL);
    {
        xjson_u32(json_input, "n_levels", &signal.n_levels);
        xjson_u32(json_input, "kernel_size", &signal.kernel_size);
        xjson_u32(json_input, "n_points", &signal.n_points);
        
        xjson_array_begin(json_input, "a_kernel");
        signal.a_kernel = (float *) malloc(signal.kernel_size * sizeof(float));
        for (int i = 0; i < signal.kernel_size; i++) {
            xjson_float(json_input, NULL, &signal.a_kernel[i]);
        }
        if (signal.kernel_size % 2 == 0) {
            printf("Warning!!! kernel_size should be odd, got %d\n", signal.kernel_size);
        }
        
        int half_kernel_size = signal.kernel_size / 2;
        signal.a_matrix = (float *) malloc(signal.n_points * signal.n_points * sizeof(float));
        for (int i = 0; i < signal.n_points; i++) {
            for (int j = 0; j < signal.n_points; j++) {
                if (((i - j) >= -half_kernel_size) && ((i - j) <= half_kernel_size)) {
                    signal.a_matrix[i * signal.n_points + j] = signal.a_kernel[i - j + half_kernel_size];
                } else {
                    signal.a_matrix[i * signal.n_points + j] = 0;
                } 
            }
        }
        xjson_array_end(json_input);

        xjson_array_begin(json_input, "xi_signal");
        signal.xi_signal = (float *) malloc(signal.n_points * sizeof(float));
        for (int i = 0; i < signal.n_points; i++) {
            xjson_float(json_input, NULL, &signal.xi_signal[i]);
        }
        xjson_array_end(json_input);

        signal.c_levels = (float *) malloc(signal.n_levels * sizeof(float));
        memset(signal.c_levels, 0, signal.n_levels * sizeof(float));
        signal.x_matrix = (float *) malloc(signal.n_levels * signal.n_points * sizeof(float));
        memset(signal.x_matrix, 0, signal.n_levels * signal.n_points * sizeof(float));
        signal.f_signal = (float *) malloc(signal.n_points * sizeof(float));
        memset(signal.f_signal, 0, signal.n_points * sizeof(float));
    }
    xjson_object_end(json_input);

    free(json_input);
    free(json_input_str);

    if (json_input->error) {
        puts(json_input->error_message);
    }

    return signal;
}


void save_signal_to_file(signal_t *signal, const char *filename) {
    char json_str[MAX_JSON_STRING_LEN];
    xjson *json_output = malloc(sizeof(xjson));
    memset(json_output, 0, sizeof(xjson));

    xjson_setup_write(json_output, true, json_str, MAX_JSON_STRING_LEN);
    xjson_object_begin(json_output, NULL);
    {
        xjson_u32(json_output, "n_levels", &signal->n_levels);
        xjson_u32(json_output, "kernel_size", &signal->kernel_size);
        xjson_u32(json_output, "n_points", &signal->n_points);
        
        xjson_array_begin(json_output, "a_kernel");
        for (int i = 0; i < signal->kernel_size; i++) {
            xjson_float(json_output, NULL, &signal->a_kernel[i]);
        }
        xjson_array_end(json_output);

        xjson_array_begin(json_output, "xi_signal");
        for (int i = 0; i < signal->n_points; i++) {
            xjson_float(json_output, NULL, &signal->xi_signal[i]);
        }
        xjson_array_end(json_output);

        
        xjson_array_begin(json_output, "c_levels");
        for (int i = 0; i < signal->n_levels; i++) {
            xjson_float(json_output, NULL, &signal->c_levels[i]);
        }
        xjson_array_end(json_output);

        
        xjson_array_begin(json_output, "x_matrix");
        for (int i = 0; i < signal->n_points * signal->n_levels; i++) {
            xjson_float(json_output, NULL, &signal->x_matrix[i]);
        }
        xjson_array_end(json_output);

        
        xjson_array_begin(json_output, "f_signal");
        for (int i = 0; i < signal->n_points; i++) {
            xjson_float(json_output, NULL, &signal->f_signal[i]);
        }
        xjson_array_end(json_output);
    }
    xjson_object_end(json_output);

    //puts(json_str);

    if (json_output->error) {
        puts(json_output->error_message);
    }

    free(json_output);

    save_string_to_file(filename, json_str);
}


signal_t copy_signal(signal_t *src_signal) {
    signal_t signal;

    signal.n_levels = src_signal->n_levels;
    signal.kernel_size = src_signal->kernel_size;
    signal.n_points = src_signal->n_points;
    
    signal.a_kernel = (float *) malloc(signal.kernel_size * sizeof(float));
    memcpy(signal.a_kernel, src_signal->a_kernel, signal.kernel_size * sizeof(float));
    if (signal.kernel_size % 2 == 0) {
        printf("Warning!!! kernel_size should be odd, got %d\n", signal.kernel_size);
    }

    signal.a_matrix = (float *) malloc(signal.n_points * signal.n_points * sizeof(float));
    memcpy(signal.a_matrix, src_signal->a_matrix, signal.n_points * signal.n_points * sizeof(float));

    signal.xi_signal = (float *) malloc(signal.n_points * sizeof(float));
    memcpy(signal.xi_signal, src_signal->xi_signal, signal.n_points * sizeof(float));

    signal.c_levels = (float *) malloc(signal.n_levels * sizeof(float));
    memcpy(signal.c_levels, src_signal->c_levels, signal.n_levels * sizeof(float));
    
    signal.x_matrix = (float *) malloc(signal.n_levels * signal.n_points * sizeof(float));
    memcpy(signal.x_matrix, src_signal->x_matrix, signal.n_levels * signal.n_points * sizeof(float));
    
    signal.f_signal = (float *) malloc(signal.n_points * sizeof(float));
    memcpy(signal.f_signal, src_signal->f_signal, signal.n_points * sizeof(float));
    
    return signal;
}


void print_signal(signal_t *signal) {
    printf("------------\n");
    printf("n_levels: %d\n", signal->n_levels);
    printf("kernel_size: %d\n", signal->kernel_size);
    printf("n_points: %d\n", signal->n_points);

    printf("a_kernel: ");
    for (int i = 0; i < signal->kernel_size; i++) {
        printf("%.2f ", signal->a_kernel[i]);
    }
    printf("\n");

    printf("a_matrix: \n");
    for (int i = 0; i < signal->n_points; i++) {
        for (int j = 0; j < signal->n_points; j++) {
            printf("%.2f ", signal->a_matrix[i * signal->n_points + j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("xi_signal: ");
    for (int i = 0; i < signal->n_points; i++) {
        printf("%.2f ", signal->xi_signal[i]);
    }
    printf("\n");

    printf("c_levels: ");
    for (int i = 0; i < signal->n_levels; i++) {
        printf("%.2f ", signal->c_levels[i]);
    }
    printf("\n");

    printf("x_matrix: \n");
    for (int i = 0; i < signal->n_points; i++) {
        printf("     ");
        for (int j = 0; j < signal->n_levels; j++) {
            printf("%.0f ", signal->x_matrix[i * signal->n_levels + j]);
        }
        printf("\n");
    }

    printf("f_signal: ");
    for (int i = 0; i < signal->n_points; i++) {
        printf("%.2f ", signal->f_signal[i]);
    }
    printf("\n");
    printf("------------\n");
}

#endif // PIECEWISE_CONSTNT_SIGNAL_H__