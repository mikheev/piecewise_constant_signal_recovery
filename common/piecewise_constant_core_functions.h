#ifndef PIECEWISE_CONSTANT_CORE_FUNCTIONS_H__
#define PIECEWISE_CONSTANT_CORE_FUNCTIONS_H__

#include "piecewise_constant_signal.h"
#include "DspLib/MatrixFunctions/arm_math.h"


static inline bool check_x_is_singular(signal_t *signal) {
    // return true if x is a singular matrix
    // retuenfalse if x is not singular
    bool no_ones;
    for (int j = 0; j < signal->n_levels; j++) {
        no_ones = true;
        for (int i = 0; i < signal->n_points; i++) {
            if ((int)(signal->x_matrix[i * signal->n_levels + j]) == 1) {
                no_ones = false;
                break;
            }
        }
        if (no_ones) {
            return no_ones;
        }
    }
    return no_ones;
}


static inline float calculate_error_func(signal_t *signal) {
    arm_matrix_instance_f32 matrix_a;
    arm_matrix_instance_f32 matrix_x;
    arm_matrix_instance_f32 matrix_ax;
    arm_matrix_instance_f32 matrix_ax_t;
    arm_matrix_instance_f32 matrix_ax_t_ax;
    arm_matrix_instance_f32 matrix_ax_t_ax_inv;
    arm_matrix_instance_f32 matrix_ax_plus;
    arm_matrix_instance_f32 matrix_pi;
    arm_matrix_instance_f32 matrix_i_m_pi;
    arm_matrix_instance_f32 matrix_error_vect;
    arm_matrix_instance_f32 matrix_xi;

    // #TODO Move this out 
    float32_t *matrix_ax_p = (float *) malloc(signal->n_points * signal->n_levels * sizeof(float));
    float32_t *matrix_ax_t_p = (float *) malloc(signal->n_points * signal->n_levels * sizeof(float));
    float32_t *matrix_ax_t_ax_p = (float *) malloc(signal->n_levels * signal->n_levels * sizeof(float));
    float32_t *matrix_ax_t_ax_inv_p = (float *) malloc(signal->n_levels * signal->n_levels * sizeof(float));
    float32_t *matrix_ax_plus_p = (float *) malloc(signal->n_points * signal->n_points * sizeof(float));
    float32_t *matrix_pi_p = (float *) malloc(signal->n_points * signal->n_points * sizeof(float));
    float32_t *matrix_i_m_pi_p = (float *) malloc(signal->n_points * signal->n_points * sizeof(float));
    float32_t *matrix_error_vect_p = (float *) malloc(signal->n_points * sizeof(float));

    arm_mat_init_f32(&matrix_a, signal->n_points, signal->n_points, (float32_t *)(signal->a_matrix));
    arm_mat_init_f32(&matrix_x, signal->n_points, signal->n_levels, (float32_t *)(signal->x_matrix));
    arm_mat_init_f32(&matrix_ax, signal->n_points, signal->n_levels, (float32_t *)matrix_ax_p);
    arm_mat_init_f32(&matrix_ax_t, signal->n_levels, signal->n_points, (float32_t *)matrix_ax_t_p);
    arm_mat_init_f32(&matrix_ax_t_ax, signal->n_levels, signal->n_levels, (float32_t *)matrix_ax_t_ax_p);
    arm_mat_init_f32(&matrix_ax_t_ax_inv, signal->n_levels, signal->n_levels, (float32_t *)matrix_ax_t_ax_inv_p);
    arm_mat_init_f32(&matrix_ax_plus, signal->n_levels, signal->n_points, (float32_t *)matrix_ax_plus_p);
    arm_mat_init_f32(&matrix_pi, signal->n_points, signal->n_points, (float32_t *)matrix_pi_p);
    arm_mat_init_f32(&matrix_i_m_pi, signal->n_points, signal->n_points, (float32_t *)matrix_i_m_pi_p);
    arm_mat_init_f32(&matrix_error_vect, signal->n_points, signal->n_points, (float32_t *)matrix_error_vect_p);
    arm_mat_init_f32(&matrix_xi, signal->n_points, 1, (float32_t *)signal->xi_signal);

    // AX = A X
    arm_mat_mult_f32(&matrix_a, &matrix_x, &matrix_ax);
    arm_mat_trans_f32(&matrix_ax, &matrix_ax_t);
    arm_mat_mult_f32(&matrix_ax_t, &matrix_ax, &matrix_ax_t_ax);
    arm_mat_inverse_f32(&matrix_ax_t_ax, &matrix_ax_t_ax_inv);
    arm_mat_mult_f32(&matrix_ax_t_ax_inv, &matrix_ax_t, &matrix_ax_plus);
    arm_mat_mult_f32(&matrix_ax, &matrix_ax_plus, &matrix_pi);

    // print_arm_matrix(&matrix_a, "A");
    // print_arm_matrix(&matrix_x, "X");
    // print_arm_matrix(&matrix_ax, "AX");
    // print_arm_matrix(&matrix_pi, "PI");

    for (int i = 0; i < signal->n_points; i++) {
        for (int j = 0; j < signal->n_points; j++) {
            matrix_i_m_pi_p[i * signal->n_points + j] = -matrix_pi_p[i * signal->n_points + j];
            if (i == j) {
                matrix_i_m_pi_p[i * signal->n_points + j] += 1.;
            }
        }
    }

    
    arm_mat_mult_f32(&matrix_i_m_pi, &matrix_xi, &matrix_error_vect);

    double error = 0;
    for (int i = 0; i < signal->n_points; i++) {
        error += matrix_error_vect_p[i] * matrix_error_vect_p[i];
    }
    

    free(matrix_ax_p);
    free(matrix_ax_t_p);
    free(matrix_ax_t_ax_p);
    free(matrix_ax_t_ax_inv_p);
    free(matrix_ax_plus_p);
    free(matrix_pi_p);
    free(matrix_i_m_pi_p);
    free(matrix_error_vect_p);

    return error;
}


static inline void evaluate_optimal_c_by_pseudoinversion(signal_t *signal) {
    arm_matrix_instance_f32 matrix_a;
    arm_matrix_instance_f32 matrix_x;
    arm_matrix_instance_f32 matrix_ax;
    arm_matrix_instance_f32 matrix_ax_t;
    arm_matrix_instance_f32 matrix_ax_t_ax;
    arm_matrix_instance_f32 matrix_ax_t_ax_inv;
    arm_matrix_instance_f32 matrix_g_plus;
    arm_matrix_instance_f32 matrix_c;
    arm_matrix_instance_f32 matrix_xi;

    // #TODO Move this out 
    float32_t *matrix_ax_p = (float *) malloc(signal->n_points * signal->n_levels * sizeof(float));
    float32_t *matrix_ax_t_p = (float *) malloc(signal->n_points * signal->n_levels * sizeof(float));
    float32_t *matrix_ax_t_ax_p = (float *) malloc(signal->n_levels * signal->n_levels * sizeof(float));
    float32_t *matrix_ax_t_ax_inv_p = (float *) malloc(signal->n_levels * signal->n_levels * sizeof(float));
    float32_t *matrix_g_plus_p = (float *) malloc(signal->n_points * signal->n_points * sizeof(float));

    arm_mat_init_f32(&matrix_a, signal->n_points, signal->n_points, (float32_t *)(signal->a_matrix));
    arm_mat_init_f32(&matrix_x, signal->n_points, signal->n_levels, (float32_t *)(signal->x_matrix));
    arm_mat_init_f32(&matrix_ax, signal->n_points, signal->n_levels, (float32_t *)matrix_ax_p);
    arm_mat_init_f32(&matrix_ax_t, signal->n_levels, signal->n_points, (float32_t *)matrix_ax_t_p);
    arm_mat_init_f32(&matrix_ax_t_ax, signal->n_levels, signal->n_levels, (float32_t *)matrix_ax_t_ax_p);
    arm_mat_init_f32(&matrix_ax_t_ax_inv, signal->n_levels, signal->n_levels, (float32_t *)matrix_ax_t_ax_inv_p);
    arm_mat_init_f32(&matrix_g_plus, signal->n_levels, signal->n_points, (float32_t *)matrix_g_plus_p);
    arm_mat_init_f32(&matrix_c, signal->n_levels, 1, (float32_t *)signal->c_levels);
    arm_mat_init_f32(&matrix_xi, signal->n_points, 1, (float32_t *)signal->xi_signal);

    // AX = A X
    arm_mat_mult_f32(&matrix_a, &matrix_x, &matrix_ax);
    arm_mat_trans_f32(&matrix_ax, &matrix_ax_t);
    arm_mat_mult_f32(&matrix_ax_t, &matrix_ax, &matrix_ax_t_ax);

    // Regularization
    for (int i = 0; i < matrix_ax_t_ax.numCols; i++) {
        matrix_ax_t_ax.pData[i * matrix_ax_t_ax.numCols + i] += 0.0001;
    }

    arm_mat_inverse_f32(&matrix_ax_t_ax, &matrix_ax_t_ax_inv);
    arm_mat_mult_f32(&matrix_ax_t_ax_inv, &matrix_ax_t, &matrix_g_plus);
    arm_mat_mult_f32(&matrix_g_plus, &matrix_xi, &matrix_c);
   
    free(matrix_ax_p);
    free(matrix_ax_t_p);
    free(matrix_ax_t_ax_p);
    free(matrix_ax_t_ax_inv_p);
    free(matrix_g_plus_p); 
}

#endif // PIECEWISE_CONSTANT_CORE_FUNCTIONS_H__